#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	srand(time(0));

	int m, n, k, numbers = 0;

	printf("m = ");
	scanf("%d", &m);

	printf("n = ");
	scanf("%d", &n);

	do {
		printf("k = ");
		scanf("%d", &k);
	} while (k < 3 || k > 10);

	for (int i = 0; i < m; i++) {
		int numberA = rand() % 126;
		printf("%4d | ", numberA);
		numbers++;
		if (numbers == k) {
			printf("\n");
			numbers = 0;
		}
	}

	for (int i = 0; i < n; i++) {
		double numberB = 3 + rand() % 3;
		numberB += (double)(rand() % 101) / 100;
		printf("%.2lf | ", numberB);
		numbers++;
		if (numbers == k) {
			printf("\n");
			numbers = 0;
		}
	}

	return 0;
}