#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	srand(time(0));


	const int n = 10;
	int a, b, arr[n], sumOfOddIndexElements = 0, sumOfElementsBetweenFirstAndLastNegativeElements = 0, indexOfFirstNegativeNumber = 0, indexOfLastNegativeNumber = 0;

	printf("a = ");
	scanf("%d", &a);

	do {
		printf("b = ");
		scanf("%d", &b);
	} while (b <= a);


	for (int i = 0; i < n; i++) {
		arr[i] = a + rand() % (b - a + 1);
		printf("%d ", arr[i]);
		if (arr[i] < 0)	indexOfLastNegativeNumber = i;
	}
	for (int i = 1; i < n; i += 2) {
		sumOfOddIndexElements += arr[i];
	}
	printf("\nsumOfOddIndexElements = %d", sumOfOddIndexElements);

	for (int i = 0; i < n; i++) {
		if (arr[i] < 0) {
			indexOfFirstNegativeNumber = i;
			break;
		}
	}
	for (int i = indexOfFirstNegativeNumber + 1; i < indexOfLastNegativeNumber; i++) {
		sumOfElementsBetweenFirstAndLastNegativeElements += arr[i];
	}
	printf("\nsumOfElementsBetweenFirstAndLastNegativeElements = %d", sumOfElementsBetweenFirstAndLastNegativeElements);


	

	return 0;
}