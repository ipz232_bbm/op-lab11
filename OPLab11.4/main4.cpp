#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {
	srand(time(0));

	double P[13], min, max;;

	for (int i = 0; i < 13; i ++) {
		P[i] = -100 + rand() % 200 + rand() % 100 / 100.;
		printf("%.2lf | ", P[i]);
	}

	min = max = P[0];

	for (int i = 1; i < 13; i++) {
		if (P[i] < min) min = P[i];
		if (P[i] > max) max = P[i];
	}

	printf("\nmax = %lf\nmin = %lf\n", max, min);

	for (int i = 0; i < 13; i++) {
		if (P[i] > 0) P[i] *= min * min;
		if (P[i] < 0) P[i] *= max * max;
		printf("%.2lf | ", P[i]);
	}

	return 0;
}