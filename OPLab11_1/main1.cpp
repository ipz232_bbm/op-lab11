#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <Windows.h>
#include <stdlib.h>
#include <time.h>

int main() {
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(0));

	int task;
	int number;
	printf("������ �����, ��� ������� �������� �������, �� ������ ���� ����������� ���������������� ����� � ������ number:\n1 - [-4; -1)\n2 - [100; 299]\n3 - [-35; -1] �����\n4 - [-128; 127]\n5 - [-7; -13) �������\n6 - [-7.85; 28*sqrt(3)]\n7 - [-100; 100]\n8 - [23; 71]\n9 - [0; 2)\n10 - [sqrt(17); sqrt(82))\n\n");
	scanf("%d", &task);

	switch (task){
	case 1:
		number = -4 + rand() % 3;
		break;
	case 2:
		number = 100 + rand()%200;
		break;
	case 3:
		do {
			number = -35 + rand() % 35;
		} while (number % 2 != 0);
		break;
	case 4:
		number = -128 + rand()%256;
		break;
	case 5:
		do {
			number = -7 + rand() % 20;
		} while (number % 2 == 0);
		break;
	case 6:
		number = -7 + rand()%36;
		break;
	case 7:
		number = -100 + rand()%201;
		break;
	case 8:
		number = 23 + rand()%95;
		break;
	case 9:
		number = rand()%2;
		break;
	case 10:
		number = 3 + rand()%13;
		break;
	}

	printf("%d\n", number);

	return 0;
}